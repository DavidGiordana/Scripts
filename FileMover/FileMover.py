from FolderTree import DirEntry, FileEntry, is_dir_entry, is_file_entry
from helper import matcher, flat_list, get_listionary_shared_values, listionary_matcher
from Listionary import Listionary
from argparse import ArgumentParser
from sys import exit
from os.path import isdir, join
from shutil import move
from os import makedirs, rmdir, listdir
from FileMoverHelper import FileMoverHelper

# Data Processing

# Obtiene los archivos a procesar dado dos árboles base
def get_files_to_process(baseTree, workingTree):
    baseChildren = baseTree.get_child_keys()
    workingChildren = workingTree.get_child_keys()

    (onlyInBase, inBoth , onlyInWorking) = matcher(baseChildren, workingChildren)
    onlyInBase = flat_list([baseTree[x].get_files() for x in onlyInBase])
    onlyInWorking = flat_list([workingTree[x].get_files() for x in onlyInWorking])

    for itemId in inBoth:
        baseItem = baseTree[itemId]
        workingItem = workingTree[itemId]

        if type(baseItem) != type(workingItem):
            onlyInBase += baseItem.get_files()
            onlyInWorking += workingItem.get_files()

        if is_dir_entry(baseItem):
            (base, working) = get_files_to_process(baseItem, workingItem)
            onlyInBase += base
            onlyInWorking += working

    return (onlyInBase, onlyInWorking)

# Obtiene la lista de archivos matcheados
def get_matched_files(onlyInBase, onlyInWorking):
    f = lambda x: "{}-{}-{}".format(x.modificationTime,
                                    x.size,
                                    x.get_extension())

    baseFiles = Listionary.load_from_list(f, onlyInBase)
    workingFiles = Listionary.load_from_list(f, onlyInWorking)

    shared = get_listionary_shared_values(baseFiles, workingFiles)
    return flat_list([process_files(b,w) for (b,w) in shared])

# Dadas dos listas de Archivos Intenta matchearlos
#
# Como condición los argumentos deben tener la misma fecha de modificación,
# tamaño y extensión.
def process_files(baseFiles, workingFiles):
    b = Listionary.load_from_list(lambda x: x.identifier, baseFiles)
    w = Listionary.load_from_list(lambda x: x.identifier, workingFiles)

    (onlyInBase, shared, onlyInWorking) = listionary_matcher(b, w)
    result = []

    # matchea de ser posible los archivos con el mismo nombre
    for (bMatch, wMatch) in shared:
        for x in bMatch:
            r = try_match(x, wMatch)
            if r:
                result.append(r)
            else:
                onlyInBase.append(x)
        onlyInWorking += wMatch

    # matchea de ser posible el resto de archivos
    for x in onlyInBase:
        r = try_match(x, onlyInWorking)
        if r:
            result.append(r)
    return result

def try_match(baseFile, workingFiles):
    count = len(workingFiles)
    if count == 0:
        return None
    if count == 1:
        return (baseFile, workingFiles.pop())

    index = ask_file_option(baseFile, workingFiles)
    if index:
        return (baseFile, workingFiles.pop(index))
    return None

# Dado un archivo y varias posibilidades de matchea, pregunta
# al usuario cómo asociarlos.
#
# Esto se debe a que no hay datos suficientes para realizar el matcheo de forma
# automática.
def ask_file_option(baseFile, workingFiles):
    print("El archivo: \n\t{}".format(baseFile.get_rel_path()))
    print("¿A cuál archivo corresponde? (Cualquier valor inválido se tomará como ninguno)")
    for (i, x) in enumerate(workingFiles):
        print("    {}) {}".format(i + 1, x.get_rel_path()))
    ans = input("> ")
    try:
        n = int(ans)
        if n > 0 and n <= len(workingFiles):
            return n
    except: pass
    return None

# Helper
#-------------------------------------------------------------------------------

# Similar a Assert
def check_condition(condition, errorMessage):
    if not condition:
        print(errorMessage)
        exit(1)

# Pregunta al usuario si quiere realizar una acción
def askYesNo(question, yes=["yes"], no=["no"], error=None):
    while True:
        answer = input("{} ({} / {}): ".format(question,
                                               ", ".join(yes),
                                               ", ".join(no)))
        if answer in yes:
            return True
        elif answer in no:
            return False
        elif error:
            print(error)

# Parser
#-------------------------------------------------------------------------------

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("--simulate",
                        action="store_true",
                        help="Simula la transferencia (No aplica cambios)")
    parser.add_argument("--base", "-b",
                        required=True,
                        help="Directorio de origen")
    parser.add_argument("--working", "-w",
                        required=True,
                        help="Directorio de trabajo")
    parser.add_argument("--ask", "-a",
                        action="store_true",
                        help="Antes de actuar muestra las transferencias a realizar")
    parser.add_argument("--file_extensions", "-f",
                        nargs="+",
                        help="Indica las extensiones de directoris que deben ser considerados como archivos (para simplificar el procesamiento)")
    return parser.parse_args()

# Main
#-------------------------------------------------------------------------------

def main():
    # Parsea argumentos
    args = parse_arguments()
    check_condition(isdir(args.base), "La ruta base no es un directorio")
    check_condition(isdir(args.working), "La ruta de trabajo no es un directorio")

    exclude = [".DS_Store"]

    if args.file_extensions == None:
        specialFiles = []
    else:
        specialFiles = args.file_extensions

    print("Generando Modelo de {}".format(args.base))
    baseTree = DirEntry.from_path(args.base, exclude, specialFiles)

    print("Generando Modelo de {}".format(args.working))
    workingTree = DirEntry.from_path(args.working, exclude, specialFiles)

    print("Procesando Archivos")
    (b, w) = get_files_to_process(baseTree, workingTree)
    matchedFiles = get_matched_files(b, w)

    if len(matchedFiles) == 0:
        print("No se hallaron archivos para mover")
        return

    fileMoverHelper = FileMoverHelper(matchedFiles)

    if args.simulate:
        fileMoverHelper.show_matches()
    elif args.ask:
        fileMoverHelper.show_matches()
        if askYesNo("¿Desea proceder?", yes=["yes", "y", "si", "s"],  no=["no", "n"]):
            fileMoverHelper.perform()
    else:
        fileMoverHelper.perform()

if __name__ == "__main__":
    main()
