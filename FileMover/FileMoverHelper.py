from FolderTree import FileEntry, DirEntry, is_file_entry
from os.path import join
from os import makedirs
from shutil import move, rmtree

'''

'''
class FileMoverHelper:

    def __init__(self, matches):
        self.matches = matches
        self.subtreeItems = []

    #  Interface Methods
    # --------------------------------------------------------------------------

    '''
    Show all matches found
    '''
    def show_matches(self):
        count = len(self.matches)
        print("Matches hallados: {}".format(count))
        for (b, w) in self.matches:
            self.__print_file_move(w.get_rel_path(), b.get_rel_path())

    '''
    Perform the operations of moving
    '''
    def perform(self):
        if len(self.matches) == 0:
            return

        for (b, w) in self.matches:
             self.__move_file(b, w)

        w = self.matches[0][1]
        self.__delete_empty_folders(w.root)

    # --------------------------------------------------------------------------

    '''
    Move a file based in a Base Node and a Working Node
    '''
    def __move_file(self, b, w):
        parentPath = join(w.root.identifier, b.parent.get_rel_path())
        newPath = join(parentPath, b.identifier)

        self.__print_file_move(w.get_rel_path(), b.get_rel_path())

        sourcePath = w.get_abs_path()
        makedirs(parentPath, exist_ok=True)
        try:
            self.__print_file_move(sourcePath, newPath)
            move(sourcePath, newPath)

            w.identifier = b.identifier
            w.self_pop()
            w.root.put_file_in_folder_path(w, b.parent.get_rel_path())
        except:
            self.__print_error_moving(sourcePath, newPath)

    '''
    Delete all empty folders of a Tree Node
    '''
    def __delete_empty_folders(self, node):
        if is_file_entry(node):
            return

        entryNames = node.get_child_keys()
        for entryName in entryNames:
            self.__delete_empty_folders(node[entryName])

        if len(node.children) == 0:
            self.__print_deletion(node.get_rel_path())
            rmtree(node.get_abs_path())
            node.self_pop()

    # Printing
    # --------------------------------------------------------------------------

    '''
    Print a moving message
    '''
    def __print_file_move(self, source, destination):
        print("> \"{}\" -> \"{}\"".format(source, destination))

    '''
    Print a moving error message
    '''
    def __print_error_moving(self, origin, destination):
        print("Error: no se ha podido renombrar el archivo \"{}\" -> \"{}\"".format(source, destination))

    '''
    Print a file or folder deletion
    '''
    def __print_deletion(self, path):
        print("Eliminando: {}".format(path))
