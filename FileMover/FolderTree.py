from GenTree import GenericTree
from os.path import isdir, isfile, join, basename, splitext, getmtime, getsize, getctime, sep
from os import listdir
import json
from helper import get_size


# Representación de nodo Directorio
class DirEntry(GenericTree):

    # Initialization
    # ------------------------------------------------------------------------ #

    '''
    Inicializador: Toma tres argumentos:
    * Identifier = identificador del elemento (nombre del directorio)
    * parent = Elemento padre. En el caso de la raiz no tiene padre
    '''
    def __init__(self, identifier, parent = None):
        if parent == None:
            super(DirEntry, self).__init__(identifier, parent, self)
        else:
            super(DirEntry, self).__init__(identifier, parent, parent.root)

    '''
    Convierte una ruta en un objeto Directorio
    Toma 1 argumento obligatorio y dos opcionales
    * path: Ruta del directorio a trabajar
    * exclude: Indica los archivos a ser omitidos durante el escaneo
    * specialFiles: Indica las extensiones de las carpetas a considerar como archivos
    * parent: Nodo Padre, None para la raiz
    '''
    def from_path(path, exclude = [], specialFiles = [], parent = None):
        # genera el identificador
        if parent == None:
            identifier = join(path, "")
        else:
            identifier = basename(path)

        # Crea el nodo
        node = DirEntry(identifier, parent)

        # Rellena la información
        for i in listdir(path):
            itemPath = join(path, i)

            # Omite archivos
            if i in exclude or i.startswith("._"):
                continue

            # Genera nodos hijos
            isSpecial = len([x for x in specialFiles if i.endswith(x)]) > 0
            if isdir(itemPath) and not isSpecial:
                node[i] = DirEntry.from_path(itemPath, exclude, specialFiles, node)
            else:
                node[i] = FileEntry.from_path(itemPath, node)
        return node

    # Paths
    # ------------------------------------------------------------------------ #

    '''
    Obtiene la ruta absoluta
    '''
    def get_abs_path(self):
        return join(self.root.identifier, self.get_rel_path())

    '''
    Obtiene la ruta relativa
    '''
    def get_rel_path(self):
        if self.is_root():
            return ""
        return join(self.parent.get_rel_path(), self.identifier)

    '''
    Dada una lista que representa la jerarquía de una ruta retorna el elemento.
    Si la lista está vacía se retorna el elemento actual.
    En caso de no existir dicho elemento se retornará None
    '''
    def get_file_by_path_list(self, pathList):
        if len(pathList) == 0:
            return self
        child = self[pathList[0]]
        if child != None:
            return child.get_file_by_path_list(pathList[1:])
        return None

    '''
    Dada una ruta retorna una lista con los componentes
    '''
    def get_path_components(self, path):
        return [x for x in path.split(sep) if len(x) > 0]

    '''
    Retorna una entrada dada una ruta
    '''
    def get_file_by_path(self, path):
        pathComponents = self.get_path_components(path)
        return self.get_file_by_path_list(pathComponents)

    '''
    Dada la ruta del directorio padre inserta un archivo.
    Si el directorio no existe lo crea
    '''
    def put_file_in_folder_path(self, file, path):
        pathComponents = self.get_path_components(path)
        self.put_file_in_folder_path_by_list(file, pathComponents)

    '''
    Dada la ruta del directorio padre en formato de lista inserta un archivo.
    Si el directorio no existe lo crea
    '''
    def put_file_in_folder_path_by_list(self, file, pathComponents):
        if len(pathComponents) == 0:
            self[file.identifier] = file
            return

        nextFolderName = pathComponents.pop(0)
        nextFolder = self[nextFolderName]

        if nextFolder == None:
            nextFolder = DirEntry(nextFolderName, self.parent)
            self[nextFolderName] = nextFolder

        nextFolder.put_file_in_folder_path_by_list(file, pathComponents)


    # Internal
    # ------------------------------------------------------------------------ #

    '''
    Representación de la estructura con forma de String
    '''
    def __repr__(self):
        return "Dir: {}".format(self.get_rel_path())

    '''
    Realiza una copia profunda del objeto
    '''
    def clone(self):
        cp = DirEntry(self.identifier, self.parent)
        for (identifier, item) in self.children.items():
            cp[identifier] = item.clone()
        return cp

    #
    # ------------------------------------------------------------------------ #

    '''
    Retorna todos los archivos contenidos
    '''
    def get_files(self):
        result = []
        for v in self.children.values():
            result += v.get_files()
        return result


'''
Representación de nodo Archivo
'''
class FileEntry(GenericTree):

    # Initialization

    '''
    Inicializador: Toma tres argumetnos:
    * Identifier = identificador del elemento (nombre del directorio)
    * parent = Elemento padre. En el caso de la raiz no tiene padre
    '''
    def __init__(self, identifier, parent):
        super(FileEntry, self).__init__(identifier, parent, parent.root)

    '''
    Convierte una ruta en un objeto Archivo
    Toma 1 argumento obligatorio y uno opcional
    * path: Ruta del archivo a trabajar
    * parent: Nodo Padre, None para la raiz
    '''
    def from_path(path, parent = None):
        identifier = basename(path)
        node = FileEntry(identifier, parent)

        node.creationTime = getctime(path)
        node.modificationTime = getmtime(path)
        node.size = get_size(path)
        return node

    # ------------------------------------------------------------------------ #
    # Paths

    '''
    Obtiene la ruta absoluta
    '''
    def get_abs_path(self):
        return join(self.parent.get_abs_path(), self.identifier)

    '''
    Obtiene la ruta relativa
    '''
    def get_rel_path(self):
        return join(self.parent.get_rel_path(), self.identifier)

    '''
    Dada una lista que representa la jerarquía de una ruta retorna el elemento.
    Si la lista está vacía se retorna el elemento actual.
    En caso de no existir dicho elemento se retornará None
    '''
    def get_file_by_path_list(self, pathList):
        if len(pathList) == 0:
            return self
        return None

    '''
    Obitene la extensión del archivo
    '''
    def get_extension(self):
        return splitext(self.identifier)[1]

    '''
    Retorna todos los archivos contenidos
    '''
    def get_files(self):
        return [self]

    '''
    Brinda una representación en forma de String del Objeto
    '''
    def __repr__(self):
        return "<File: {}>".format(self.get_rel_path())

    '''
    Realiza una copia profunda del objeto
    '''
    def clone(self):
        cp = FileEntry(self.identifier, self.parent)
        cp.creationTime = self.creationTime
        cp.modificationTime = self.modificationTime
        cp.size = self.size
        return cp


def is_dir_entry(item):
    return type(item) == DirEntry

def is_file_entry(item):
    return type(item) == FileEntry
