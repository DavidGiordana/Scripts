
class GenericTree:

    def __init__(self, identifier, parent = None, root = None):
        self.parent = parent
        self.children = {}
        self.identifier = identifier
        if parent == None:
            self.root = self
        else:
            self.root = root

    #---------------------------------------------------------------------------

    def __getitem__(self, key):
        return self.children.get(key)

    def __setitem__(self, key, value):
        self.children[key] = value

    #---------------------------------------------------------------------------

    def self_pop(self):
        if self.parent != None and self.identifier in self.parent.children:
            self.parent.children.pop(self.identifier)

    def leaves_count(self):
        if self.is_leaf():
            return 1
        return sum([v.leavesCount() for (_, v) in self.children.items()])

    def get_child_keys(self):
        return list(self.children.keys())

    def is_leaf(self):
        return len(self.children) == 0

    def is_root(self):
        return self.root == self

    def get_leaves(self):
        if self.is_leaf():
            return [self]
        result = []
        for item in self.children.values():
            if item.is_leaf():
                result.append(item)
            else:
                result += item.get_eaves()
        return result

    def get_siblings(self):
        if self.is_root():
            return []
        siblings = self.parent
        return [siblings[k] for k in siblings.children if k != self.identifier]

    def get_height(self):
        if self.is_root():
            return 0
        return 1 + self.parent.get_height()

    def count_descendence(self):
        return sum([x.count_descendence() for x in self.children])

    def isSiblingOf(self, node):
        return self != node and self.parent == node.parent

    def print_tree(self, deepth = 0):
        prompt = "   |" * (max((deepth - 1), 0))
        print(prompt + ">" + self.identifier)
        for (_,v) in self.children.items():
            if v != None:
                v.printTree(deepth + 1)
