
class Listionary:

    def __init__(self):
        self.__data = {}

    def __getitem__(self, key):
        return self.__data.get(key)

    def __setitem__(self, key, value):
        if value == None:
            return
        values = self.__data.get(key)
        if values == None:
            values = []
        values.append(value)
        self.__data[key] = values

    def __repr__(self):
        keys = self.keys()
        if len(keys) == 0:
            data = ""
        else:
            data = "\n".join(["{} -> {}".format(k, self[k]) for k in keys])
        return "<Listionary{}>".format(data)

    def pop(self, key, value):
        values = self[key]
        if values != None and value in values:
            values.remove(value)
            return True
        return False

    def pop_all_values_of_key(self, key):
        oldValues = self[key]
        self.__data[key] = None
        return oldValues

    def keys(self):
        return self.__data.keys()

    def load_from_list(f, lst):
        result = Listionary()
        for x in lst:
            result[f(x)] = x
        return result
