try:
    from os import scandir
except ImportError:
    from scandir import scandir

from os.path import getsize, isdir
import itertools

# Dada una ruta indica el tamaño del contenido
def get_size(path):
    if isdir(path):
        files = [x.path for x in scandir(path)]
        return sum([getsize(x) for x in files])
    else:
        return getsize(path)

# Elimina un elemento de una lista devolviendo un booleano como resultado
def pop_item_from_list(item, list):
    index = None
    for (i, element) in enumerate(list):
        if element == item:
            index = i
            break
    if index == None:
        return False
    list.pop(index)
    return True

# Dadas dos listas matchea sus componentes
# [a] -> [a] -> ([a], [a], [a])
# El sesultado tiene la forma ((onlyInList1, inBoth, onlyInList2))
def matcher(list1, list2):
    lst1 = list(list1)
    lst2 = list(list2)

    onlyIn1 = []
    inBoth = []

    while len(lst1) > 0:
        item1 = lst1.pop(0)
        if pop_item_from_list(item1, lst2):
            inBoth.append(item1)
        else:
            onlyIn1.append(item1)
    return (onlyIn1, inBoth, lst2)

# Dada una lista de lista, retorna una lista plana
def flat_list(lst):
    return list(itertools.chain.from_iterable(lst))

# Dados dos Listionarys retorna un match basado en la clave de la misma forma
# que lo hace la función matcher
#
# Listionary a -> Listionary a -> ([a], [(a,a)], [a])
def listionary_matcher(list1, list2):
    keysOnlyIn1, keysInBoth, keysOnlyIn2 = matcher(list1.keys(), list2.keys())
    onlyIn1 = [list1[x] for x in keysOnlyIn1]
    onlyIn2 = [list2[x] for x in keysOnlyIn2]
    inBoth = [(list1[x], list2[x]) for x in keysInBoth]
    return (flat_list(onlyIn1), inBoth, flat_list(onlyIn2))

# Dados dos Lictionarys retorna solo los valores matcheados (compartidos)
def get_listionary_shared_values(list1, list2):
    return listionary_matcher(list1, list2)[1]
