import re
import requests
from urllib.request import urlopen
from bs4 import BeautifulSoup
from os import listdir, rename
from os.path import join, splitext, basename
from argparse import ArgumentParser
from json import loads
import unicodedata

#------------------------------------------------------------------------------#
# Download Episodes from OnePlace.com
#
# Dependencies:
#   * bs4
#
# The cookies needs to be in json format:
# { "cookies": [ {"name": ..., "value": ..., ...}, ...]}
# To get this results I used the Mozilla Firefox plug-in "cookie-manager."
# To run use python 3.x
# To know the usage run with -h flag
#------------------------------------------------------------------------------#
# Donload Section

CHUNK = 16 * 1024

# Download the content of an URL to destination path
def download_file(url, destinationName):
    response = urlopen(url)
    with open(destinationName, 'wb') as f:
        while True:
            chunk = response.read(CHUNK)
            if not chunk:
                break
            f.write(chunk)

# Model for a podcat Episode
#------------------------------------------------------------------------------#

class PodcastEpisode:

    def __init__(self, title, pageUrl, cookies):
        self.title = title
        self.pageUrl = pageUrl
        self.cookies = cookies

    # Returns the mp3 file URL
    def getMp3Url(self):
        page = requests.get(self.pageUrl, cookies=self.cookies)
        (_, temp1) = page.text.split("fileUrl: '",1)
        (temp2, _) = temp1.split("',",1)
        return temp2

    # Save the file into a destination Folder
    def downloadMP3(self, destinationFolder):
        filename = self.title + ".mp3"

        # Downloads the file
        temporalDestinationPath = join(destinationFolder, "." + filename)
        download_file(self.getMp3Url(), temporalDestinationPath)

        # Rename the downloaded file
        destinationPath = join(destinationFolder, filename)
        rename(temporalDestinationPath, destinationPath)

    # -----------------

    def downloadEpisodes(episodes, destinationFolderPath):
        count = len(episodes)
        print("Se descargarán {} item(s)".format(count))
        for index, episode in enumerate(episodes):
            print("Descargando item {} de {}: {}".format(index + 1, count, episode.title))
            episode.downloadMP3(destinationFolderPath)

#------------------------------------------------------------------------------#

def getListOfEpisodes(baseUrl, cookies, count = 1):
    print("Intentando obtener información de la página {}".format(count))
    url = baseUrl + "?page={0}".format(count)
    page = requests.get(url).text
    soup = BeautifulSoup(page, "html.parser")
    rawEpisodes = soup.find_all(attrs={"data-lt": "archiveEpisode"})
    episodes = [__htmlToPodcastEpisode(ep, cookies) for ep in rawEpisodes]
    if len(episodes) > 0:
        episodes += getListOfEpisodes(baseUrl, cookies, count = count + 1)
    return episodes


def __htmlToPodcastEpisode(html, cookies):
    url = html["href"]
    title = html.find(attrs={"class": "title"}).text
    return PodcastEpisode(title, url, cookies)


#------------------------------------------------------------------------------#
# Cookies

def getCookies(path):
    if path == None:
        return {}
    with open(path, "r") as f:
        content = f.read()
        json = loads(content)
        cookies = json["cookies"]
        result = {}
        for cookie in cookies:
            name = cookie["name"]
            value = cookie["value"]
            result[name] = value
        return result

# Helper
#------------------------------------------------------------------------------#

def get_filename(path):
    return splitext(basename(path))[0]

def filter_str(str, f):
    return "".join([c for c in list(str) if f(c)])

def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
                  if unicodedata.category(c) != 'Mn')

def get_title(str):
    return filter_str(strip_accents(str.lower()), lambda c: c.isalnum())

def filter_to_download_episodes(destinationPath, availableEpisodes):
    downloadedFiles = set([get_title(get_filename(x)) for x in listdir(destinationPath)])
    return [ep for ep in availableEpisodes if get_title(ep.title) not in downloadedFiles]

#------------------------------------------------------------------------------#
# Parser

# Parse the user input
def parseInformation():
    parser = ArgumentParser()
    parser.add_argument("--url", "-u", required = True, type= str, help="Url de la página de descargas")
    parser.add_argument("--destination", "-d", required = True, type= str, help="Ruta de la carpeta de descargas")
    parser.add_argument("--cookies", "-c", required=True, type=str, help="Ruta del archivo con las cookies")
    parser.add_argument("--simulate", "-s", action="store_true", help="Indica que las operaciones solo se simularán (no se descargará ningún archivo)")
    return parser.parse_args()

#------------------------------------------------------------------------------#
# Main Function

def main():
    # Get the basic information
    args = parseInformation()
    cookies = getCookies(args.cookies)

    # Get all list of episodes
    availableEpisodes = getListOfEpisodes(args.url, cookies)

    # Filter the downloaded Episodes
    availableEpisodes = filter_to_download_episodes(args.destination, availableEpisodes)

    if args.simulate:
        print("")
        print("Items a descargar {}".format(len(availableEpisodes)))
        for ep in availableEpisodes:
            print("> {}".format(ep.title))
    else:
        PodcastEpisode.downloadEpisodes(availableEpisodes, args.destination)

if __name__ == "__main__":
    main()
