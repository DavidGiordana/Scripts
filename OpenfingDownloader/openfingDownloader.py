from argparse import ArgumentParser
from selenium import webdriver
from time import sleep
import json
from os.path import isfile, isdir, join
from os import makedirs
from urllib.request import urlretrieve

# Lista los elementos html referentes a los cursos
def list_courses_elements(driver):
    coursesElement = driver.find_element_by_css_selector("div#courses div div#AllCourses")
    return coursesElement.find_elements_by_css_selector("a")

# Dada una lista lista de elementos html relacionados a un curso obtiene la inforamción
# básica de cada uno en formato de diccionario de la forma:
# { título: [Curso]}
def retrieve_courses_data(coursesElements, driver):
    result = {}
    for courseElement in coursesElements:
        print("Procesando curso {}".format(courseElement.text))
        courseElement.click()
        sleep(2)
        classesElements = driver.find_elements_by_css_selector("div#Classes div.video a")
        classesData = retrieve_course_data(classesElements, driver)
        if len(classesData) > 0:
            result[courseElement.text] = classesData

    return result

# Dada una lista de elementos html relacionados a las clases de un curso
# retorna una lista con el título de cada clase y la url del video
def retrieve_course_data(classesElements, driver):
    if len(classesElements) == 0:
        return []

    classesElements[0].click()
    sleep(2)
    videoElement = driver.find_element_by_css_selector("video#vPlayer source")
    url = videoElement.get_attribute("src")
    
    return [(classElement.text, convert_video_url(url, i + 1)) for i, classElement in enumerate(classesElements)]

# Retorna un diccionario con la iformación de los cursos
def retrieve_courses():
    url = "http://open.fing.edu.uy/"

    print("Abriendo navegador")
    driver = webdriver.Firefox()

    print("Cargando página a procesar")
    driver.get(url)

    print("Obteniendo lista de cursos")
    coursesElements = list_courses_elements(driver)

    data = retrieve_courses_data(coursesElements, driver)

    driver.close()
    return data


# Dada una url base y un número construye la url de un video
def convert_video_url(videoUrl, number):
    return videoUrl.replace("_01.", "_%02d." % number)

# Guarda la inforamción en un archivo json
def save_courses_data(data, path):
    with open(path, "w+") as f:
        f.write(json.dumps(data, indent=4))

def load_courses_data_from_file(path):
    with open(path) as f:
        return json.loads(f.read())

def download_if_needed(url, downloadPath, message):
    if isfile(downloadPath):
         return

    tempPath = downloadPath + ".part"
    print(message)
    try:
        urlretrieve(url, tempPath)
        os.reaname(tempPath, downloadPath)
    except:
        print("Ha habido un problema al descargar el archivo")

def download_courses(courses, destinationDirPath):
    if not isdir(destinationDirPath):
        print("{} no es un directorio".format(destinationDirPath))


    destinationDirPath = join(destinationDirPath, "openfing")
    for (courseName, classes) in courses.items():
        courseDirPath = join(destinationDirPath, courseName)
        count = len(classes)
        digits = len(str(count))

        if not isdir(courseDirPath):
            makedirs(courseDirPath)

        for (i, (title, downloadLink)) in enumerate(classes):
            downloadPath = join(courseDirPath, "%0{}d - {}.mp4".format(digits, title)) % (i + 1)
            message = "Descargando clase {}/{} del curso {}: {}".format(i+1, count, courseName, title)

            download_if_needed(downloadLink, downloadPath, message)





# ------------------------------------------------------------

# Parsea los argumentos de la linea de comandos
def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("--load", "-l", help="Carga un arhcivo con la información obtenida")
    parser.add_argument("--download", "-d", help="Descarga los videos del los cursos en el directorio ingresado")
    parser.add_argument("--save", "-s", help="Guarda la información obtenida en un archivo")
    return parser.parse_args()

# ------------------------------------------------------------

def main():
    args = parse_arguments()

    # Evita guardar un archivo que había sido cargado
    if args.load != None and args.save != None:
        return

    # Carga la inforamción
    if args.load == None:
        courses = retrieve_courses(path)
    else:
        path = args.load

        if not isfile(path):
            print("No existe el archivo {}".format(path))
            return

        try:
            courses = load_courses_data_from_file(path)
        except:
            print("No se pudo leer el archivo {}".format(path))
            return

    if args.save != None:
        save_courses_data(args.save)

    if args.download != None:
        download_courses(courses, args.download)
    
    

if __name__ == "__main__":
    main()

