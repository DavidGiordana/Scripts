# Serie Info Getter

Obtiene información y aplica acciones sobre series

## Dependencias:
* send2trash
* ffprobe (en linea de comandos)
El script solo trabaja bajo `python 3.x`

## Argumentos del Script
Comandos | Función
--- | ---
`--source`, `-s` | Toma uno o varios direcotorios de origen. **(Requerido)**
`--delete`, `-d`| Indica que el directorio de la serie debe ser eliminado.
`--save` | Indica que se guardará la información de la serie en formato JSON.
`--show`| Muestra la infomración de la serie en formato JSON.
`--interactive`, `-i` | Ejecuta el script en modo interactivo.

## Argumentos del modo interactivo
Dentro del modo interactivo se aceptan los siguientes argumentos.

Comandos | Función
--- | ---
`--delete`, `-d` | Indica que el directorio de la serie debe ser eliminado.
`--show` | Muestra la infomración de la serie en formato JSON.
`--save` | Indica que se guardará la información de la serie en formato JSON.
`--next`, `-n` | Pasa a procesar el siguiente archivo (si existe).
`--exit`, `-e` | Termina la ejecución del script.
`--clear`, `-c` | Limpia la pantalla.