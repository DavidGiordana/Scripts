from os import listdir
from os.path import join, getctime, getmtime, getsize, isdir, split
from subprocess import PIPE, Popen, STDOUT
from argparse import ArgumentParser, ArgumentError
import json
import os
from send2trash import send2trash
from platform import system
from sys import exit

#------------------------------------------------------------------------------#
# Retrieve and/or save the information of a serie and delete a serie
#
# Dependencies:
#   * send2trash
#   * ffprobe (in command line)
#
# Interactive Options:
#   * --delete -d       Deletes the directory
#   * --show            Shows the file tree
#   * --save            Saves the information file
#   * --next -n         Process the next file
#   * --exit -e         Exits
#   * --clear -c        Clears the screen
#
# Python version 3.x
# Author: David Giordana
#------------------------------------------------------------------------------#

def getVideoDuration(path):
    comm = 'ffprobe "%s" -show_format 2>&1 | sed -n "s/duration=//p";' % path
    ps = Popen(comm, shell=True, stdout=PIPE, stderr=STDOUT)
    output = ps.communicate()[0]
    duration = output.decode("utf-8").rstrip("\n")
    try:
        return int(float(duration))
    except:
        return None

#------------------------------------------------------------------------------#

class SerieModel:

    def __init__(self, path):
        if path.endswith(os.sep):
            path = path[:-1]
        (self.parentDirectory, self.title) = split(path)
        self.content = self.__scanFolder(path)

    def __scanFolder(self, path):
        tree = {}
        for item in listdir(path):
            itemPath = join(path, item)
            if isdir(itemPath):
                tree[item] = self.__scanFolder(itemPath)
            else:
                duration = getVideoDuration(itemPath)
                # It is a video
                if duration != None:
                    itemData = {"creationTime": getctime(itemPath),
                                "modificationTime": getmtime(itemPath),
                                "size": getsize(itemPath),
                                "duration": duration
                    }
                    tree[item] = itemData
        return tree

    def getJsonString(self, save):
        jsonString = json.dumps(self.__dict__, indent=4, ensure_ascii=False)
        if save:
            self._saveJson(jsonString)
        return jsonString

    def _saveJson(self, jsonString):
        destination = join(self.parentDirectory, self.title + ".json")
        with open(destination, "w+") as f:
            f.write(jsonString)

# Parsers
#------------------------------------------------------------------------------#

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("--source", "-s", nargs="+", required=True, help="Directorio de origen")
    parser.add_argument("--delete", "-d", action="store_true", help="Indica que se quiere elimiar el directorio")
    parser.add_argument("--show", action="store_true", help="Muestra el arbol de archivos")
    parser.add_argument("--save", action="store_true", help="Guarda el archivo en el directorio padre al origen")
    parser.add_argument("--interactive", "-i", action="store_true", help="Inicia una sesión interactiva")
    return parser.parse_args()

def create_interactive_parser():
    parser = ArgumentParser()
    parser.add_argument("--delete", "-d", action="store_true", help="Indica que se quiere eliminar el directorio")
    parser.add_argument("--show", action="store_true", help="Muestra el arbol de archivos")
    parser.add_argument("--save", action="store_true", help="Guarda el archivo en el directorio padre al origen")
    parser.add_argument("--next", "-n", action="store_true", help="Pasa a procesar el siguiente archivo")
    parser.add_argument("--exit", "-e", action="store_true", help="Sale del programa")
    parser.add_argument("--clear", "-c", action="store_true", help="Limpia la pantalla")
    return parser

# Helper
#------------------------------------------------------------------------------#

def check_source(source):
    b = isdir(source)
    if not b:
        print("Error: El archivo \"{}\" no es un directorio".format(source))
    return b

def clear_screen():
    if system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")

# Working
#------------------------------------------------------------------------------#

def single_worker(args, source):
    if args.show or args.save:
        serieModel = SerieModel(source)
        jsonString = serieModel.getJsonString(args.save)
        if args.show:
            print(jsonString)

    if args.delete:
        send2trash(source)

def interactive_worker(source, parser):
    serieModel = None
    while True:
        commands = input("{} > ".format(source))
        if commands == "": continue
        try:
            args = parser.parse_args(commands.split())
            if args.clear:
                clear_screen()
            if args.next:
                break
            if args.exit:
                exit(0)
            if args.show or args.save:
                if not serieModel:
                    serieModel = SerieModel(source)
                jsonString = serieModel.getJsonString(args.save)
                if args.show:
                    print(jsonString)
            if args.delete:
                send2trash(source)
                break
        except ArgumentError as e:
            print(e)

# Main
#------------------------------------------------------------------------------#

def main():
    # Process the input
    args = parse_arguments()
    sources = [x for x in args.source if check_source(x)]

    # perform the tasks
    if args.interactive:
        parser = create_interactive_parser()
        for source in sources:
            interactive_worker(source, parser)
    else:
        for source in sources:
            single_worker(args, source)

if __name__ == '__main__':
    main()
