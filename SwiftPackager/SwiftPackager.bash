# Script para crear, compilar y correr paquetes swift
#
# Script creado por David Giordana 2017

# Función para imprimir la ayuda
function printHelp {
    clear
    echo "Este script está diseñado para poder crear, compilar y correr"
    echo "paquetes swift."
    echo ""
    echo "Los comandos seran de la forma \"bash SwiftPackager.bash <Comando> <Argumentos>\""
    echo "Donde los comandos disponibles son:"
    echo ""
    echo "create: Crea un paquete"
    echo "      Arg1: Nombre del paquete a crear"
    echo "      Arg2: Ruta de la carpeta donde se realizará el trabajo (Sin incluir el nombre)"
    echo ""
    echo "compile: Compila un paquete"
    echo "      Arg1: Ruta de la carpeta donde se encuentra el paquete"
    echo ""
    echo "run: Ejecuta un paquete compilado"
    echo "      Arg1: Ruta del paquete"
    echo ""
    echo "destroy:  Destruye un paquete. ADVERTENCIA: esta operación no requiere de confirmación y elimina permanentemente el pauqete junto con su contenido"
    echo "      Arg1: Ruta de la carpeta donde se encuentra el paquete"
    echo ""
    echo "Script creado por David Giordana 2017"
}

# Compila un paquete
#
# Argumentos:
#   Ruta del paquete
function compilePackage {
    if [ -d $1 ]; then
        current_dir=$(pwd)
        packagePath="$(realpath $1)"
        packageName="${packagePath##*/}"
        compiledPath="$packagePath/.build/debug/$packageName"
        cd $packagePath
        swift build && cp $compiledPath $packagePath
        cd $current_dir
    else
        echo "ERROR el directiorio \"$1\" no existe"
    fi
}

# Corrre un elmento compilado de un paquete
#
# Argumentos:
#   Ruta del paquete
function runPackage {
    packagePath="$(realpath $1)"
    packageName="${packagePath##*/}"
    compiledPath="$packagePath/.build/debug/$packageName"
    if [ ! -d $packagePath ]; then
        echo "ERROR: El paquete \"$1\" no existe"
    elif [ -f $compiledPath ]; then
        $compiledPath
    else
        echo "ERROR: El paquete \"$1\" no está compilado"
    fi
}

# Crea un paquete en vacio
#
# Argumentos:
#   Nombre del paquete
#   Ruta del directorio donde se creará el paquete
function createPackage {
    # Calcula el directorio de trabajo
    current_dir=$(pwd)
    package_directory="$(realpath $2)/$1"
    sources_direcotory="$package_directory/Sources"
    package_file="$package_directory/Package.swift"
    main_file="$sources_direcotory/main.swift"
    # Hace las comprobaciones necesarias antes de trabajar
    if [  -d $package_directory ]; then
        echo "ERROR: El directorio \"$package_directory\" ya existe"
    else
        # Crea el directorio del paquete
        echo "Creando directorio del paquete"
        mkdir $package_directory
        # Entra al directorio del paquete
        echo "Entrando al directorio del paquete"
        cd $package_directory
        # Crea el directorio de recursos
        mkdir $sources_direcotory
        echo "Creaando el directorio de recursos"
        # Crea el archivo del paquete
        echo "Creando Package.swift"
        echo "import PackageDescription" >> $package_file
        echo "" >> $package_file
        echo "let package = Package(" >> $package_file
        echo "    name: \"$1\"" >> $package_file
        echo ")" >> $package_file
        # Crea el archivo main
        echo "Creando el archivo main"
        echo "import Foundation" >> $main_file
        echo "" >> $main_file
        echo "print(\"Hello World!\")" >> $main_file
        # Termina el trabajo
        echo "Terminado."
    fi
    cd $current_dir
}

# Elimina o destruye un pauqete
#
# Argumentos:
#   Ruta del paquete
function destroyPackage {
    packagePath="$(realpath $1)"
    if [ ! -d $packagePath ]; then
        echo "ERROR: El paquete \"$1\" no existe"
    else
        rm -rf $packagePath
    fi
}

if [ $# -lt 2 ]; then
    printHelp
else
    case $1 in
        compile)
            if [ $# -ge 2 ]; then
                compilePackage $2
            else
                echo "ERROR: la función \"$1\" requere 1 argumento"
            fi
            ;;
        destroy)
            if [ $# -ge 2 ]; then
                destroyPackage $2
            else
                echo "ERROR: la función \"$1\" requere 1 argumento"
            fi
            ;;
        run)
            if [ $# -ge 2 ]; then
                runPackage $2
            else
                echo "ERROR: la función \"$1\" requere 1 argumento"
            fi
            ;;
        create)
            if [ $# -ge 3 ]; then
                createPackage $2 $3
            else
                echo "ERROR: la función \"$1\" requere 2 argumentos"
            fi
            ;;
        *)
            echo "ERROR: El comando \"$1\" no existe"
    esac
fi
