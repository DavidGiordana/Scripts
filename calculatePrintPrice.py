from argparse import ArgumentParser
from math import ceil

def parseArguments():
    parser = ArgumentParser()
    parser.add_argument("--resma", type=float, required=True, help="Precio de resma de 500 hojas")
    parser.add_argument("--toner", type=float, required=True, help="Precio de toner x 1000 hojas")
    parser.add_argument("--paginas", type=int, required=True, help="Cantidad de páginas para imprimir")
    parser.add_argument("--esprial_precio", type=float, default=10, help="Precio de una espiral")
    parser.add_argument("--esprial_hojas", type=int, default=60, help="Cantidad de hojas por espiral")
    parser.add_argument("--ganancia_monto", type=float, default=0, help="Ganancia deseada en pesos")
    parser.add_argument("--ganancia_porcentaje", type=int, default=0, help="Ganancia deseada en porcenaje")
    return parser.parse_args()

def main():
    args = parseArguments()
    reamPrice = args.resma
    tonerPrice = args.toner
    pagesCount = args.paginas
    spiralPrice = args.esprial_precio
    sheetsPerSpiral = args.esprial_hojas
    earningAmount = args.ganancia_monto
    earningPercentage = args.ganancia_porcentaje

    sheetsCount = ceil(pagesCount / 2)
    reamCost = round((sheetsCount / 500) * reamPrice, 2)

    tonerCount = pagesCount / 1000
    inkCost = round(tonerCount * tonerPrice, 2)

    spiralCount = ceil(sheetsCount / sheetsPerSpiral)
    spiralCost = round(spiralCount * spiralPrice, 2)

    costs = round(reamCost + inkCost + spiralCost, 2)
    earningsByPercentage = costs * earningPercentage / 100
    earnings = round(earningsByPercentage + earningAmount, 2)

    total = round(costs + earnings, 2)
    
    print(f"Cantidad de hojas: {sheetsCount}")
    print(f"Cantidad de espirales: {spiralCount}")
    print(f"Cantidad de toners: {tonerCount}")
    print("-"*30)
    print(f"Costo de papel: ${reamCost}")
    print(f"Costo de toner: ${inkCost}")
    print(f"Costo de espirales: ${spiralCost}")
    print("-"*30)
    print(f"Ganancias: ${earnings}")
    print("-"*30)
    print(f"Total: ${total}")


if __name__ == "__main__":
    main()