from argparse import ArgumentParser
from os import listdir, rename
from os.path import join, isfile

def getFiles(directoryPath):
    files = [x for x in listdir(directoryPath) if not ".DS_Store" in x] 
    files.sort()
    return files


def renamer(directoryPath, startNumber):
    files = getFiles(directoryPath)
    digits = len(str(len(files)))
    counter = startNumber

    for filename in files:
        sourceFile = join(directoryPath, filename)
        number = str(counter).zfill(digits)
        newFilename = "{} - {}".format(number, filename)
        destinationFile = join(directoryPath, newFilename)
        
        rename(sourceFile, destinationFile)
        
        counter += 2

    

def parseArguments():
    parser = ArgumentParser()
    parser.add_argument("--first", "-f", required=True, type=str, help="First directory to merge")
    parser.add_argument("--seccond", "-s", required=True, type=str, help="Seccond directory to merge")
    return parser.parse_args()

def main():
    args = parseArguments()
    renamer(args.first, 1)
    renamer(args.seccond, 2)

if __name__ == "__main__":
    main()