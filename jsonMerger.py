from argparse import ArgumentParser
from send2trash import send2trash
from os.path import isfile, isdir, join, realpath
from json import dumps, loads
from os import listdir
from sys import exit
import re

def readJsonFile(filePath: str):
    if not isfile(filePath):
        return {}

    with open(filePath, "r") as f:
        fileContent = f.read()
    return loads(fileContent)

def writeJonFile(data: any, filePath: str):
    fileContent = dumps(data, indent=4)
    
    with open(filePath, "w") as f:
        f.write(fileContent)

def replaceIgnoreCase(text: str, target: str, replacement: str) -> str:
    pattern = re.compile(target, re.IGNORECASE)
    return pattern.sub(replacement, text)

# Returns (filename, path)
def listJsonFiles(directoryPath: str, targetPath: str) -> [(str, str)]:
    filenames = [x for x in listdir(directoryPath) if filterFilename(x)]
    files = [(x, join(directoryPath, x)) for x in filenames]
    return [(filename, filePath) for (filename, filePath) in files if filePath != targetPath]

def filterFilename(filename: str) -> bool:
    filename = filename.lower()
    return filename.endswith(".json") and not filename.startswith(".")

def processJsonFiles(files: [(str, str)], targetPath: str) -> any:
    result = readJsonFile(targetPath)

    for (filename, path) in files:
        print(f"Processing {filename}")
        key = replaceIgnoreCase(filename, ".json", "")
        content = readJsonFile(path)
        result[key] = content

    return result

def deleteFiles(files: [(str, str)]):
    for (filename, path) in files:
        print(f"Deleting {filename}")
        send2trash(path)

def parseArguments():
    parser = ArgumentParser()
    parser.add_argument("--source", "-s", required=True, help="The source directory to analyze")
    parser.add_argument("--target", "-t", required=True, help="The target json file")
    return parser.parse_args()

def main():
    args = parseArguments()
    sourcePath: str = realpath(args.source)
    targetPath: str = realpath(args.target)

    files = listJsonFiles(sourcePath, targetPath)
    jsonData = processJsonFiles(files, targetPath)
    writeJonFile(jsonData, targetPath)

    deleteFiles(files)

if __name__ == "__main__":
    main()