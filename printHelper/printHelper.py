import re
from os import listdir, makedirs
from os.path import join, isdir, expanduser
from argparse import ArgumentParser
from math import ceil
from subprocess import call
import PyPDF2 as pdf
import os
import sys


def open_file(path):
    if sys.platform.startswith('darwin'):
        call(('open', path))
    elif os.name == 'nt': # For Windows
        os.startfile(path)
    elif os.name == 'posix': # For Linux, Mac, etc.
        call(('xdg-open', path))

def get_numbers(pattern):
    sections = pattern.split(",")
    result = []
    for sections in sections:
        components = sections.split(":")
        if len(components) == 1:
            result.append(int(components[0]))
        else:
            components = [int(x) for x in components]
            lo = min(components)
            hi = max(components)
            result += [i for i in range(lo, hi + 1)]
    return result

class PrinterHelper:

    def __init__(self, sourceFile):
        self.sourceFile = sourceFile
        self.__open_pdf()
        self.__prepare_env()
        self.__define_parser()

    def __define_parser(self):
        self.parser = ArgumentParser(add_help=False)
        self.parser.add_argument("--odd", "-o", type=str, help="Imprime páginas impares")
        self.parser.add_argument("--even", "-e", type=str, help="Imprime páginas pares")
        self.parser.add_argument("--quit", "-q", action="store_true", help="Termina la sesión")
        self.parser.add_argument("--help", "-h", action="store_true", help="Muestra este mensaje de ayuda")
        self.parser.add_argument("--simulate", action="store_true", help="Muestra los archivos a imprimir pero no imprime nada")

    def __prepare_env(self):
        home = expanduser("~")
        self.workingDir = join(home, ".ph")

        if not isdir(self.workingDir):
            makedirs(self.workingDir)

    def __open_pdf(self):
        self.pdf = pdf.PdfFileReader(self.sourceFile)
        self.pagesCount = self.pdf.getNumPages()
    
        self.oddCount = int(ceil(self.pagesCount / 2.0))
        self.evenCount = int(self.pagesCount / 2.0)

    def __print_status(self):
        print("Páginas Impares: {} | Páginas pares: {}".format(self.oddCount, self.evenCount))

    def __build_pdf_and_open(self, indexes):
        pdf_out = pdf.PdfFileWriter()
        for i in indexes:
            pdf_out.addPage(self.pdf.getPage(i))

        destinationPath = join(self.workingDir, "toPrint.pdf")
        with open(destinationPath, "wb") as out_stream:
            pdf_out.write(out_stream)
        open_file(destinationPath)
        


    def __prepafe_for_print(self, indexes, isEven, simulate=False):
        toPrintIndexes = []
        for index in indexes:
            pageIndex = (index - 1) * 2
            if isEven:
                pageIndex += 1

            if pageIndex >= self.pagesCount or pageIndex < 0:
                continue

            print("Imprimiendo página {}".format(pageIndex + 1))
            toPrintIndexes.append(pageIndex)         

        if not simulate:   
            self.__build_pdf_and_open(toPrintIndexes)

    def execute(self):
        while True:
            self.__print_status()

            userInput = input("> ")
            namespace, args = self.parser.parse_known_args(userInput.split())

            if len(args) > 0:
                print("No se ha encontrado comando válido en {}".format(userInput))
            elif namespace.quit:
                break
            elif namespace.help:
                self.parser.print_help()
            elif namespace.odd != None:
                self.__prepafe_for_print(get_numbers(namespace.odd), False, namespace.simulate)
            elif namespace.even != None:
                self.__prepafe_for_print(get_numbers(namespace.even), True, namespace.simulate)
            

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("--source", "-s", required=True, help="Directorio con imágenes a imprimir")
    return parser.parse_args()


def main():
    args = parse_arguments()
    print("Presione -h para obtener ayuda")
    PrinterHelper(args.source).execute()

if __name__ == "__main__":
    main()
