from argparse import ArgumentParser
from sys import exit
import xml.etree.ElementTree as ET
from xml.dom import minidom

# Helper
# ------------------------------------------------------------------------------

class FunctionalQueue:

    def __init__(self, function, queue):
        self.function = function
        self.queue = queue
        self.index = 0

    def getItem(self, parameter):
        while self.index < len(self.queue):
            if self.function(parameter, self.index):
                item = self.queue[self.index]
                self.index += 1
                return item
            self.index += 1
        return None

note_id_2_note = {
    0: "c",
    1: "c#",
    2: "d",
    3: "d#",
    4: "e",
    5: "f",
    6: "f#",
    7: "g",
    8: "g#",
    9: "a",
    10: "a#",
    11: "b"
}

note_2_note_id = {
    "c"     : 0,
    "c#"    : 1,
    "d"     : 2,
    "d#"    : 3,
    "e"     : 4,
    "f"     : 5,
    "f#"    : 6,
    "g"     : 7,
    "g#"    : 8,
    "a"     : 9,
    "a#"    : 10,
    "b"     : 11
}

def even(n):
    return n % 2 == 0

def isSharp(note):
    return "#" in note

# ------------------------------------------------------------------------------

class VMPKKeymapBuilder:

    def __init__(self, baseOctave, initialOctave, initialNote, keys, sharped):
        self.baseOctave = baseOctave
        self.initialOctave = initialOctave
        self.noteOffset = note_2_note_id[initialNote]
        self.initialKeyNumber = (self.initialOctave - self.baseOctave) * len(note_id_2_note) + self.noteOffset
        self.keys = keys

        choiceFunc = lambda x, idx: isSharp(x) == (even(idx) == sharped)
        self.keyGetter = FunctionalQueue(choiceFunc, keys)


    def getKeyIndex(self, idx):
        return self.initialKeyNumber + idx

    def getNoteId(self, idx):
        return (self.noteOffset + idx) % len(note_id_2_note)

    def getKeyMap(self):
        keymap = []
        for i in range(len(self.keys)):
            keyIndex = self.getKeyIndex(i)
            note = note_id_2_note[self.getNoteId(i)]
            key = self.keyGetter.getItem(note)
            if key == None:
               break
            keymap.append((keyIndex, key))
        return keymap

    def getAndWriteFile(self, destination):
        keymap = self.getKeyMap()
        xml = ET.Element("keyboardmap", {"version":"1.0"})
        for (keyIndex, key) in keymap:
            ET.SubElement(xml, "mapping", {"key": key, "note": str(keyIndex)})

        if not destination.endswith(".xml"):
            destination += ".xml"
        xmlstr = minidom.parseString(ET.tostring(xml)).toprettyxml(indent="   ")
        with open(destination, "w+") as f:
            f.write(xmlstr)

# ------------------------------------------------------------------------------

# True está alterado (#)
def requestKeys():
    return input("Ingrese las teclas a usar para el mapa: ")


def parseNote(noteString):
    noteString = noteString.lower()
    if len(noteString) < 2:
        return None
    note = noteString[0]
    noteString = noteString[1:]
    if note < "a" or note > "g":
        return None
    if noteString[0] == "#":
        noteString = noteString[1:]
        note += "#"
    try:
        octave = int(noteString)
        return (note, octave)
    except:
        return None

# Parsing & Error
# ------------------------------------------------------------------------------

def parseArguments():
    parser = ArgumentParser()
    parser.add_argument("--base_note", "-bn", required=True, help="Nota base a considerar")
    parser.add_argument("--base_octave", "-bo", type=int, required=True, help="Octava base de vmdk")
    parser.add_argument("--destination", "-d", required=True, help="Archivo de destino")
    return parser.parse_args()

def checkAssert(condition, message):
    if not condition:
        print("Error: " + message)
        exit(-1)

# Main
# ------------------------------------------------------------------------------

def main():
    args = parseArguments()
    n = parseNote(args.base_note)
    checkAssert(n != None, "La nota \"{0}\" no es un patrón válido".format(args.base_note))
    (note, octave) = n
    keys = requestKeys()
    keymapBuilder = VMPKKeymapBuilder(args.base_octave - 1, octave, note, keys, isSharp(note))
    keymapBuilder.getAndWriteFile(args.destination)

if __name__ == "__main__":
    main()
